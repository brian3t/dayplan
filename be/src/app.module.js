import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import { FsController } from './fs/fs.controller';

@Module({
  imports: [],
  controllers: [AppController, FsController],
  providers: [AppService],
})
export class AppModule {}
