import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule,
    {
      // logger: ['error', 'warn']
      logger: ['error', 'warn', 'verbose']
    });
  app.enableCors();
  console.info(`Staring backend server at port 3002`)
  await app.listen(3002);
}
bootstrap();
