import {Bind, Body, Controller, Get, Post} from '@nestjs/common';
import {readFileSync, writeFileSync} from 'fs'
import {resolve} from "path"
import {fs, stat} from 'node:fs'

@Controller('fs')
export class FsController {
  /**
   *
   * @param createBlkDaynoteDTO Data Transfer Object; {blocks_copy: an array of blocks, pl_blocks_copy: an array of planned blocks, daynote_copy: day's note}
   * @return {res: number, msg: string, data: number}
   */
  @Post()
  @Bind(Body())
  async create(createBlkDaynoteDTO){
    console.log(`fs create called`)
    console.log(`createBlkDaynoteDTO`, createBlkDaynoteDTO)
    if (! (createBlkDaynoteDTO instanceof Object)) return {res: 1, msg: `Must send an object array of blocks. Payload was: ` + JSON.stringify(createBlkDaynoteDTO)}
    try {
        const blocks_db_filepath = resolve(__dirname, '../../db/blocks_dn.json')
        /*if (!fs.exists(blocks_db_filepath)) {
            fs.mkdirSync('../../db')//todo this code doesn't work yet
        }*/
      writeFileSync(blocks_db_filepath, JSON.stringify(createBlkDaynoteDTO), 'utf8')
    } catch (e) {
      return {res: -1, msg: `Err: ${JSON.stringify(e)}`}
    }
    return {res: 1, msg: 'Saved blk, daynote to file system', data: createBlkDaynoteDTO.length}
  }

  @Get()
  findAll(){
    console.log(`fs get called`)
    console.log(`current dir: `, __dirname)
    const blocks_dn = readFileSync(resolve(__dirname, `../../db/blocks_dn.json`), {encoding: 'utf8', flag: 'r'})
    return JSON.stringify(blocks_dn)
  }
}
