import { Test } from '@nestjs/testing';
import { FsController } from './fs.controller';

describe('Fs Controller', () => {
  let controller;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [FsController],
    }).compile();

    controller = module.get(FsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
