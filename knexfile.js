// Update with your config settings.

module.exports = {
  development: {
    client: 'sqlite3',
    connection: {
      filename: `day.sqlite3`
    },
    useNullAsDefault: true,
    migrations: {
      directory: './migrations'
    },
  },

  staging: {
    client: 'sqlite3',
    connection: {
      filename: './day.sqlite3'
    },
    useNullAsDefault: true
  },

  production: {
  }

};
