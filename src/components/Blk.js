import React, {forwardRef, useEffect} from 'react';
import moment from 'moment'
import {fm_time_simple} from "../helper"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {solid} from "@fortawesome/fontawesome-svg-core/import.macro"


/**
 * A block. Which is 30 minutes
 * Each block is [start, task, status]
 */
const Blk = forwardRef(({block, change_block, del_block, push_block, swap_blocks, idx}, ref) => {
  // const [status, setStatus] = useState([block[2]])
  // let [task, setTask] = useState([])
  // {start, task, handleReset, child_func}
  useEffect(
    () => {
    }, [block]
  )
  const allow_drop = function (e){
    e.preventDefault();
  }

  /**
   * A change was triggered.
   * Save all tasks/all status to storage.
   */
  const change_triggered = function (){

  }
  const change_task = function (new_task){
    block[1] = new_task
    // block[2] = '' 3/7/23 Do not reset status. Future: make this configurable?
    change_block(idx, block)
    change_triggered()
  }

  const dropped = function (e){
    console.log(`dropped e: `, e)
    const dragged_idx = JSON.parse(e.dataTransfer.getData('text'))
    console.log(`idx: `)
    swap_blocks(dragged_idx, idx)
  }

  const onChangeStatus = (new_stat) => {
    console.info(new_stat)
    block[2] = new_stat
    change_block(idx, block)
    // setStatus(new_stat)
  }
  const start = block && block[0], task = block && block[1], status = block && block[2] || ''
  const end = moment(start, 'HHmm').add(30, 'minutes')
  const start_fmt = fm_time_simple(start)
  const end_fmt = fm_time_simple(end)

  let className = 'tab-list-item blk';

  return (
    <div
      className={className} onDragOver={e => allow_drop(e)} onDrop={e => dropped(e)}
    >
      <span className="timespan w-25 flex-column">{start_fmt}_{end_fmt}</span>
      <input type="text" className="w-10 status" maxLength="1" onChange={e => onChangeStatus(e.target.value)} value={status}
             onFocus={e => e.target.select()}></input>
      <input type="text" className="w-35 task" placeholder="task" value={task} onChange={e => change_task(e.target.value)} onBlur={e => change_task(e.target.value)} />
      {/*<button className="w-10 btn btn-sm btn-outline-danger" onClick={() => change_task('')}>Reset</button>*/}
      <button className="btn btn-sm w-10 btn-outline-info" draggable="true" onDragStart={e => e.dataTransfer.setData('text', JSON.stringify(idx))} tabIndex="-1">
        {/*<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M278.6 9.4c-12.5-12.5-32.8-12.5-45.3 0l-64 64c-9.2 9.2-11.9 22.9-6.9 34.9s16.6 19.8 29.6 19.8h32v96H128V192c0-12.9-7.8-24.6-19.8-29.6s-25.7-2.2-34.9 6.9l-64 64c-12.5 12.5-12.5 32.8 0 45.3l64 64c9.2 9.2 22.9 11.9 34.9 6.9s19.8-16.6 19.8-29.6V288h96v96H192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l64 64c12.5 12.5 32.8 12.5 45.3 0l64-64c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8H288V288h96v32c0 12.9 7.8 24.6 19.8 29.6s25.7 2.2 34.9-6.9l64-64c12.5-12.5 12.5-32.8 0-45.3l-64-64c-9.2-9.2-22.9-11.9-34.9-6.9s-19.8 16.6-19.8 29.6v32H288V128h32c12.9 0 24.6-7.8 29.6-19.8s2.2-25.7-6.9-34.9l-64-64z"/></svg>*/}
        <FontAwesomeIcon icon={solid('up-down-left-right')} />
      </button>
      <button className="btn btn-sm w-10 btn-outline-info push_btn" onClick={() => {
        push_block(idx)
      }} tabIndex="-1">P
      </button>
      <button className="btn btn-sm w-10 btn-outline-info del_btn" onClick={() => {
        del_block(idx)
      }} tabIndex="-1">X
      </button>
    </div>
  )
})

export default Blk;
