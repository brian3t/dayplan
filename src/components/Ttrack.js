import React, {forwardRef, useEffect, useState} from 'react';

/**
 * A Time Tracking element. Start from zero and grow to infinity.
 * cat: Category, e.g. hygiene
 */
const Ttrack = forwardRef(({cat, init_num_of_5min = 0}, ref) => {
  const [status, setStatus] = useState([])
  let [num_of_5min, set_num_of_5min] = useState([init_num_of_5min])
  useEffect(
    () => {
    }, [init_num_of_5min]
  )
  const onChangeStatus = (new_stat) => {
    new_stat = new_stat.nativeEvent.value
    console.info(new_stat)
    setStatus(new_stat)
  }
  let className = 'tab-list-item ttrack';


  return (
    <div
      className={className}
    >
      <span className="flex-column title">{cat}</span>&nbsp;
      <input disabled type="number" step="1" value={num_of_5min} maxLength="2" />&nbsp;
      <button className="btn btn-sm btn-outline-danger" onClick={() => set_num_of_5min(Number.parseInt(num_of_5min) + 1)}>+</button>
    </div>
  )
})

export default Ttrack;
