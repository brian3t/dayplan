import axios from "axios"

//Initial states template
import Blk from "./components/Blk"
import moment from "moment"
import Ttrack from "./components/Ttrack"
import {fm_time_simple, swal} from "./helper"
import {useEffect, useRef, useState} from "react"
import C from './conf'
import _ from "lodash";
import {useHotkeys} from 'react-hotkeys-hook'

const INIT_STATES = [
  ['0630', '', '', ''], ['0700', '', ''], ['0730', '', ''], ['0800', '', ''], ['0830', '', ''], ['0900', '', ''], ['0930', '', ''], ['1000', '', ''], ['1030', '', ''], ['1100', '', ''], ['1130', '', ''], ['1200', '', ''], ['1230', '', ''], ['1300', '', ''], ['1330', '']
  , ['1400', '', ''], ['1430', '', ''], ['1500', '', ''], ['1530', '', ''], ['1600', '', ''], ['1630', '', ''], ['1700', '', ''], ['1730', '', ''], ['1800', '', ''], ['1830', '', ''], ['1900', '', ''], ['1930', '', ''], ['2000', '', ''], ['2030', '', ''], ['2100', '']
  , ['2130', '', ''], ['2200', '', '']
]
const START_IDX = 0
const TASK_IDX = 1
const STATUS_IDX = 2

console.table(C)

export default App;

function App() {
  const child_ref = useRef()
  const [blocks, set_blocks] = useState(INIT_STATES)
  const [planned_blocks, set_planned_blocks] = useState(INIT_STATES)
  const [day_note, set_day_note] = useState(' ')
  const [compact_mode, set_compact_mode] = useState()

  useEffect(() => {
    // if (compact_mode){
    // }
    console.log(`compact_mode: `, compact_mode)
    load_from_json_file()
  }, [compact_mode])

  // to be called by children
  function change_block(i, new_block) {
    let new_blocks = [...blocks]
    new_blocks[i] = new_block
    set_blocks(new_blocks)
  }

  // Change a planned block. To be called by children
  function change_pl_block(i, new_block) {
    let new_blocks = [...planned_blocks]
    new_blocks[i] = new_block
    set_planned_blocks(new_blocks)
  }


  /**
   * to be called by children
   * All blocks afterward get pushed down 1
   * Set block for cur index to empty
   * @param i Current index
   */
  function push_block(i) {
    const blocks_copy = _.cloneDeep(blocks)
    const first_portion = _.cloneDeep(blocks_copy.slice(0, i))
    const second_portion = _.cloneDeep(blocks_copy.slice(i))
    const second_portion_shifted = _.cloneDeep(second_portion)
    second_portion_shifted[0][1] = ''
    second_portion_shifted[0][2] = ''
    for (let j = 1; j < second_portion_shifted.length; j++) {
      second_portion_shifted[j][1] = second_portion[j - 1][1] //same time, but task is delayed
      second_portion_shifted[j][2] = second_portion[j - 1][2] //same time, but status is delayed
    }

    let new_blocks = first_portion.concat(second_portion_shifted)
    //set block for cur idx to empty
    set_blocks(new_blocks)
  }

  /**
   * to be called by children
   * All blocks afterward get pushed down 1
   * Set block for cur index to empty
   * @param i Current index
   */
  function del_block(i) {
    const blocks_copy = _.cloneDeep(blocks)
    blocks_copy.forEach((block, index) => {
      if (index < i || index >= (blocks_copy.length - 1)) return
      blocks_copy[index][TASK_IDX] = blocks_copy[index + 1][TASK_IDX] //shift task backward
      blocks_copy[index][STATUS_IDX] = blocks_copy[index + 1][STATUS_IDX] //shift backward
    })
    blocks_copy[blocks_copy.length - 1][TASK_IDX] = '' //clear the last block
    blocks_copy[blocks_copy.length - 1][STATUS_IDX] = '' //clear the last block
    set_blocks(blocks_copy)
  }

  const reset = function () {
    return false
  }
  const hand_reset_task = function () {
    const new_blocks = _.cloneDeep(INIT_STATES)
    const new_planned_blocks = _.cloneDeep(INIT_STATES)
    set_blocks(new_blocks)
    set_planned_blocks(new_planned_blocks)
  }

  async function export_plain_text() {
    let res = []
    res = blocks.map(block => {
      const start = block[0], task = block[1], status = block[2]
      const end = moment(start, 'HHmm').add(30, 'minutes')
      const start_fmt = fm_time_simple(start)
      const end_fmt = fm_time_simple(end)

      return `${start_fmt}-${end_fmt}: ` + task + ' ' + status
    })
    res = res.join("\n")
    navigator.clipboard.writeText(res);
    await swal('Copied', 'Copied plain text to clipboard!', 500)
    await swal('Blocks as JSON', JSON.stringify(blocks), 6000)
  }

  function load_from_hardcode_json() {
    let input_block_text = '[["0600","evs 1"],["0630","evs 2"],["0700","evs 3"],["0730","evs 4"],["0800","evs 5 meet"],["0830","evs 6 prepare lod bto"],["0900","evs 7 meet"],["0930","evs 8"],["1000","evs 9"],["1030","evs 10"],["1100","clean"],["1130","lunch 1"],["1200","lunch 2"],["1230","lunch 3"],["1300",""],["1330",""],["1400",""],["1430",""],["1500",""],["1530",""],["1600",""],["1630",""],["1700",""],["1730",""],["1800",""],["1830",""],["1900",""],["1930",""],["2000",""],["2030",""],["2100",""],["2130",""],["2200",""],["2230",""]]'
    let input_blocks
    try {
      input_blocks = JSON.parse(input_block_text)
    } catch (e) {
      swal('bad json')
      return 'bad json'
    }
    set_blocks(input_blocks)
  }

  async function load_from_json_file() {
    if (!C.be) {
      swal(`Error: missing backend config`)
      return false
    }
    let {data: blocks_daynote, info} = await axios.get(`${C.be}fs`)
    try {
      blocks_daynote = JSON.parse(blocks_daynote)
    } catch (e) {
    }
    // noinspection PointlessBooleanExpressionJS
    if (!blocks_daynote || (false == blocks_daynote instanceof Object)) {
      swal('bad json response, not an object')
      return 'bad json'
    }
    let blocks_copy, pl_blocks_copy, daynote_copy
    ({blocks_copy, pl_blocks_copy, daynote_copy} = blocks_daynote)
    if (blocks_copy) set_blocks(blocks_copy)
    if (pl_blocks_copy) set_planned_blocks(pl_blocks_copy)
    set_day_note(daynote_copy)
  }

  /**
   * Save json to remote inp/blocks.json, if we have a `be` url in conf
   */
  async function save_to_json_file() {
    // noinspection JSIgnoredPromiseFromCall
    if (!C.be) {
      // noinspection JSIgnoredPromiseFromCall
      await swal('Error: missing backend config')
      return false
    }
    let blocks_copy = _.cloneDeep(blocks)
    let pl_blocks_copy = _.cloneDeep(planned_blocks)
    let daynote_copy = _.cloneDeep(day_note)
    // const save_res = await fetch(C.be + 'fs', {method: 'POST', mode: 'no-cors', body: JSON.stringify(blocks_copy)})
    const {data: save_res, info} = await axios({
      method: 'POST',
      url: `${C.be}fs`,
      withCredentials: false,
      data: {blocks_copy, pl_blocks_copy, daynote_copy}
    });


    // const {data: save_res, info} = await axios(C.be + 'fs', {method: 'POST', body: JSON.stringify(blocks_copy), mode: 'no-cors'})
    console.table(save_res)
    // noinspection JSIgnoredPromiseFromCall
    await swal('Success', 'Saved to json file' + JSON.stringify(save_res), 600)
  }

  /**
   * Swap two blocks
   * Each block is [time, task, status]
   * @param dragged_idx Initial block
   * @param dest_idx Destination block
   */
  const swap_blocks = function (dragged_idx, dest_idx) {
    /*
        let new_blocks = [...blocks]
        const temp = Object.assign(blocks[dragged_idx])
        const dragged_task = temp[1]
        const dragged_status = temp[2]
        new_blocks[dragged_idx][1] = blocks[dest_idx][1]
        new_blocks[dragged_idx][2] = blocks[dest_idx][2]
        new_blocks[dest_idx][1] = dragged_task
        new_blocks[dest_idx][2] = dragged_status
        set_blocks(new_blocks)
    */
    let new_blocks = _.cloneDeep(blocks)
    new_blocks[dragged_idx][1] = blocks[dest_idx][1]
    new_blocks[dragged_idx][2] = blocks[dest_idx][2]
    new_blocks[dest_idx][1] = blocks[dragged_idx][1]
    new_blocks[dest_idx][2] = blocks[dragged_idx][2]
    set_blocks(new_blocks)
  }
  useHotkeys('alt+c', save_to_json_file, {enableOnContentEditable: true, enableOnFormTags: true})

  return (
    <div>
      {/*<Tabs>*/}
      <div label="Plans">
        <div className="buttons">
          <button className="btn btn-outline-danger" onClick={hand_reset_task}>Reset All</button>
          {/*<button className="btn btn-success" onClick={load_from_hardcode_json}>Load From Hardcode Json</button>*/}
          <button className="btn btn-success" onClick={load_from_json_file}>Load From Json File</button>
          <button className="btn btn-success" onClick={save_to_json_file}>Save to Json File</button>
          {/*<button className="btn btn-success" onClick={export_plain_text}>To Plain Text</button>*/}
          <span className="form-check form-check-inline">
            <input id="compact_mode_inp" value={compact_mode} className="form-check-input" type="checkbox"
                   onChange={(e) => set_compact_mode(e.target.checked)}></input>
            <label className="form-check-label" htmlFor="compact_mode_inp">Compact Mode</label>
          </span>
          {/*<button className="btn btn-outline-danger" onClick={() => child_ref.current.alert_child_func()}>Reset</button>*/}
        </div>
        <div className="blocks float-start" compact_mode={compact_mode?.toString()}>
          {blocks && (blocks instanceof Array) && blocks.map((block, i) => (
            <Blk change_block={change_block} push_block={push_block} swap_blocks={swap_blocks} del_block={del_block} block={block} idx={i}
                 key={i}></Blk>
          ))}
        </div>
        <div className="blocks float-start" compact_mode={compact_mode?.toString()}>
          <hr className=""/>
          {planned_blocks && (planned_blocks instanceof Array) && planned_blocks.map((pl_block, i) => (
            <Blk change_block={change_pl_block} push_block={push_block} swap_blocks={swap_blocks} del_block={del_block} block={pl_block} idx={i}
                 key={i}></Blk>
          ))}
        </div>

        <div id="ttracks" className="position-absolute">
          <textarea value={day_note} onChange={e => set_day_note(e.target.value)} placeholder="Daily note" cols="80"
                    rows="5">{day_note}</textarea>
          <Ttrack cat="hygiene"></Ttrack>
          <Ttrack cat="food"></Ttrack>
        </div>

      </div>
      {/*<div label="Projects">
          Projects (future)
        </div>
        <div label="Settings">
          Settings (future)
        </div>*/}
      {/*</Tabs>*/}
    </div>
  );
}


