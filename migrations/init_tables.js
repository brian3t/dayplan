const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./day.sqlite3');

exports.up = function (knex, Promise){
    // console.log(`knex.schema.hasTable('x3req')`, await knex.schema.hasTable('x3req'))
    return knex.schema.createTable("blk", tbl => {
        tbl.increments("id").primary();
        tbl.text("start", 25).notNullable();
        tbl.text("task", 255);
        tbl.bool("finished")
        tbl.text("note", 2000)
        tbl.dateTime("created_at").defaultTo(knex.fn.now());
        tbl.dateTime("updated_at")
    })

};

exports.down = function (knex){
    return db.serialize(function (){
        if (knex.schema.hasTable('blk')) db.run("DROP TABLE IF EXISTS blk");
    });
};
console.log(`Done migrating init_tables.js`)
